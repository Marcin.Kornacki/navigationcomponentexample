package com.mkornacki.navigationexample.data

enum class AuthenticationState {
    UNAUTHENTICATED,
    AUTHENTICATED
}