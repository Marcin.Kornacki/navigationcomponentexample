package com.mkornacki.navigationexample.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(val street: String, val number: Int, val city: String) : Parcelable