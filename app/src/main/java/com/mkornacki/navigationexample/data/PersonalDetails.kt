package com.mkornacki.navigationexample.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class PersonalDetails(
    val firstName: String,
    val lastName: String,
    val phoneNumber: String
) : Parcelable