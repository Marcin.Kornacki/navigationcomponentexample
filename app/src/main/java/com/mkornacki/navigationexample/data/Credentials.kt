package com.mkornacki.navigationexample.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Credentials(val login: String, val password: String) : Parcelable