package com.mkornacki.navigationexample.data

data class Account(
    val personalDetails: PersonalDetails,
    val address: Address,
    val credentials: Credentials,
    var balance: Int = 0
)