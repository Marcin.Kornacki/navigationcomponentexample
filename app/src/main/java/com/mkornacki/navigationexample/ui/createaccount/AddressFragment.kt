package com.mkornacki.navigationexample.ui.createaccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.data.Address
import kotlinx.android.synthetic.main.address_fragment.*
import kotlinx.android.synthetic.main.personal_details_fragment.nextButton


class AddressFragment : Fragment() {

    val args: AddressFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.address_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nextButton.setOnClickListener {
            val personalDetails = args.personalDetails
            val address = Address(
                street.text.toString(),
                houseNumber.text.toString().toInt(),
                city.text.toString()
            )
            val direction =
                AddressFragmentDirections.actionAddressFragmentToCredentialsFragment(
                    personalDetails,
                    address
                )
            findNavController().navigate(direction)
        }
        abortButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_addressFragment_to_personalDetailsFragment
            )
        )
    }
}
