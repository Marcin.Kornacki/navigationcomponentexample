package com.mkornacki.navigationexample.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.repository.AccountRepository
import kotlinx.android.synthetic.main.balance_fragment.*


class BalanceFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.balance_fragment, container, false)
    }

    override fun onResume() {
        super.onResume()
        balance.text = "${AccountRepository.account?.balance}₪"
    }

    companion object {
        fun newInstance() = BalanceFragment()
    }
}
