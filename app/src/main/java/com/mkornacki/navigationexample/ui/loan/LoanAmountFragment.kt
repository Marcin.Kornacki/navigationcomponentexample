package com.mkornacki.navigationexample.ui.loan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.repository.AccountRepository
import kotlinx.android.synthetic.main.loan_amount_fragment.*


class LoanAmountFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.loan_amount_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        proceedButton.setOnClickListener {
            AccountRepository.topUp(shekels.text.toString().toInt())
            val direction =
                LoanAmountFragmentDirections.actionLoanAmountFragmentToDashboardFragment2()
            findNavController().navigate(direction)
        }
    }
}
