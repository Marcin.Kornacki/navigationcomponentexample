package com.mkornacki.navigationexample.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.data.AuthenticationState
import com.mkornacki.navigationexample.data.Credentials
import com.mkornacki.navigationexample.repository.AccountRepository
import kotlinx.android.synthetic.main.sign_in_fragment.*


class SignInFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.sign_in_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signInButton.setOnClickListener {
            signIn(login.text.toString(), password.text.toString())
        }
        createAccountButton.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_signInFragment_to_nav_graph_new_account))
    }

    private fun signIn(login: String, password: String) {
        AccountRepository.signIn(Credentials(login, password))
        if (AccountRepository.authenticationState == AuthenticationState.AUTHENTICATED) {
            findNavController().navigate(R.id.dashboardFragment)
        } else {
            Toast.makeText(context, "Invalid login or password", Toast.LENGTH_SHORT).show()
        }
    }
}
