package com.mkornacki.navigationexample.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mkornacki.navigationexample.R
import kotlinx.android.synthetic.main.loan_fragment.*


class LoanFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.loan_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loanButton.setOnClickListener {
            findNavController().navigate(R.id.loanTermsAndConditionsFragment)
        }
    }

    companion object {
        fun newInstance() = LoanFragment()
    }
}
