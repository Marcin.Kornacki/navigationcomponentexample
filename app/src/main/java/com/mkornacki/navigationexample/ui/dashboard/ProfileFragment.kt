package com.mkornacki.navigationexample.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.repository.AccountRepository
import kotlinx.android.synthetic.main.profile_fragment.*


class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        AccountRepository.account?.let {
            firstName.text = it.personalDetails.firstName
            lastName.text = it.personalDetails.lastName
            phoneNumber.text = it.personalDetails.phoneNumber
        }
    }

    companion object {
        fun newInstance() = ProfileFragment()
    }
}
