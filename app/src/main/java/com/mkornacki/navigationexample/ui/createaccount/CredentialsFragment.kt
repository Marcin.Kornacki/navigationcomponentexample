package com.mkornacki.navigationexample.ui.createaccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.data.Account
import com.mkornacki.navigationexample.data.Credentials
import com.mkornacki.navigationexample.repository.AccountRepository
import kotlinx.android.synthetic.main.address_fragment.*
import kotlinx.android.synthetic.main.credentials_fragment.*
import kotlinx.android.synthetic.main.credentials_fragment.abortButton


class CredentialsFragment : Fragment() {

    val args: CredentialsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.credentials_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createAccountButton.setOnClickListener {
            val personalDetails = args.personalDetails
            val address = args.address
            val credentials = Credentials(login.text.toString(), password.text.toString())
            AccountRepository.createAccount(Account(personalDetails, address, credentials))
            val direction =
                CredentialsFragmentDirections.actionCredentialsFragmentToSuccessDialogFragment()
            findNavController().navigate(direction)
        }
        abortButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(
                R.id.action_credentialsFragment_to_personalDetailsFragment
            )
        )
    }
}
