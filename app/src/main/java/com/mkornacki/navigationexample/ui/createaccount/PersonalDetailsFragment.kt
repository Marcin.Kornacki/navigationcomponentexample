package com.mkornacki.navigationexample.ui.createaccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mkornacki.navigationexample.R
import com.mkornacki.navigationexample.data.PersonalDetails
import kotlinx.android.synthetic.main.personal_details_fragment.*


class PersonalDetailsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.personal_details_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        nextButton.setOnClickListener {
            val personalDetails = PersonalDetails(
                firstName.text.toString(),
                lastName.text.toString(),
                phoneNumber.text.toString()
            )
            val direction =
                PersonalDetailsFragmentDirections.actionPersonalDetailsFragmentToAddressFragment(
                    personalDetails
                )
            findNavController().navigate(direction)
        }
    }
}
