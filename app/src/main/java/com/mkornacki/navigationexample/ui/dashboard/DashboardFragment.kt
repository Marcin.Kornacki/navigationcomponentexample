package com.mkornacki.navigationexample.ui.dashboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mkornacki.navigationexample.R
import kotlinx.android.synthetic.main.dashboard_fragment.*


class DashboardFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.dashboard_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadFragment(BalanceFragment.newInstance())
        bottomNavigation.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_balance -> {
                    loadFragment(BalanceFragment.newInstance())
                    true
                }
                R.id.action_loan -> {
                    loadFragment(LoanFragment.newInstance())
                    true
                }
                R.id.action_profile -> {
                    loadFragment(ProfileFragment.newInstance())
                    true
                }
                else -> true
            }
        }
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.pageContainer, fragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }
}
