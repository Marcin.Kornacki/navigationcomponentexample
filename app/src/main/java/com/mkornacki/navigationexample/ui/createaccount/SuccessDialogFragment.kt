package com.mkornacki.navigationexample.ui.createaccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.mkornacki.navigationexample.R
import kotlinx.android.synthetic.main.success_dialog_fragment.*


class SuccessDialogFragment : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.success_dialog_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        okButton.setOnClickListener {
            val direction =
                SuccessDialogFragmentDirections.actionSuccessDialogFragmentToPersonalDetailsFragment()
            findNavController().navigate(direction)
        }
    }
}
