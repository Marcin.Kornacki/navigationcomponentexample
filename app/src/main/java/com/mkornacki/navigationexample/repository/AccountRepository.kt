package com.mkornacki.navigationexample.repository

import com.mkornacki.navigationexample.data.*

object AccountRepository {

    var account: Account? = null
        private set

    private var database = mutableListOf(
        Account(
            PersonalDetails("Janusz", "Tracz", "123 456 789"),
            Address("Przedsiębiorcza", 11, "Szwagropolice"),
            Credentials("", ""),
            1000
        )
    )

    var authenticationState = AuthenticationState.UNAUTHENTICATED
        private set

    fun signIn(credentials: Credentials) {
        database.forEach {
            if (credentials == it.credentials) {
                account = it
                authenticationState = AuthenticationState.AUTHENTICATED
            }
        }
    }

    fun signOut() {
        authenticationState = AuthenticationState.UNAUTHENTICATED
    }

    fun createAccount(account: Account) {
        database.add(account)
    }

    fun topUp(shekels: Int) {
        account?.let {
            it.balance += shekels
        }
    }
}